﻿/*
 * Author:Pol
 * Date:3-4-2023
 */

namespace PolAlsinaP6M03UF5
{
    /// <summary>
    /// Clase per l'execucio del ex2
    /// </summary>
    class ConvertirADouble
    {
        /// <summary>
        /// converteix qualsevol string a double
        /// </summary>
        /// <param name="input">param a pasar a double</param>
        /// <returns>retorn el param convertit a double</returns>
        public static double aDoubleoU(string input)
        {
            try
            {
                return Convert.ToDouble(input);
            }
            catch (FormatException e)
            {
                Console.WriteLine(e.Message);
                return 1.0;
            }
        }
    }
}
