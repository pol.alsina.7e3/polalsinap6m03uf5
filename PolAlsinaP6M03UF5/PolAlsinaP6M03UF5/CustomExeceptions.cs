﻿/*
 * Author:Pol
 * Date:3-4-2023
 */

namespace PolAlsinaP6M03UF5
{
    /// <summary>
    /// clase personalitzada de execpcions
    /// </summary>
    public class CustomExeceptions: Exception
    {
        /// <summary>
        /// Excepcio quan element es null
        /// </summary>
        public string ElementIsNull
        {
            get
            {
                return "Element cannot be null";
            }
        }
        /// <summary>
        /// Excepcio quan element no estroba i no es pot treure
        /// </summary>
        public string RemoveElement
        {
            get
            {
                return "Element cannot be removed not find";
            }
        }
    }
}
