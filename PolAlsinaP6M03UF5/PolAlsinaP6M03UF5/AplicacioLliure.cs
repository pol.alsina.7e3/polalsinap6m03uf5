﻿/*
 * Author:Pol
 * Date:3-4-2023
 */

namespace PolAlsinaP6M03UF5
{
    /// <summary>
    /// Clase per l'execucio del ex4
    /// </summary>
    public class AplicacioLliure
    {
        List<string> llista = new List<string>();
        /// <summary>
        /// Metode que controla el menu 
        /// </summary>
        public void MenuController() {
            bool finish;
            do
            {
                Console.WriteLine("Introdueix una comanda (afegir, esborrar o sortir):");
                finish = MenuOptions();
            } while (finish != true);
        }
        /// <summary>
        /// Metode que retorna un boolea. Si es true tancar el programa
        /// </summary>
        /// <returns>Retorna un boolea</returns>
        bool MenuOptions()
        {
            string answer = Console.ReadLine().ToLower();
            switch (answer)
            {
                case "afegir":
                    AddElement();
                    break;
                case "esborrar":
                    DeleteElement();
                    break;
                case "sortir":
                    return true;
                default:
                    Console.WriteLine("resposta no trobada");
                    break;
            }
            return false;
        }
        /// <summary>
        /// intenta afeigir un element a la llista si es null lleça excepcio
        /// </summary>
        void AddElement()
        {
            Console.WriteLine("Introdueix un element per afegir:");
            string element = Console.ReadLine();
            
            try
            {
                if (String.IsNullOrEmpty(element))
                {
                    throw new CustomExeceptions();
                }
                else
                {
                    llista.Add(element);
                    Console.WriteLine("Afegit");
                }
            }
            catch (CustomExeceptions e)
            {
                Console.WriteLine(e.ElementIsNull);
            }
            
                      
        }
        /// <summary>
        /// intenta eliminar un element de la llista si no el troba lleça excepcio
        /// </summary>
        void DeleteElement()
        {
            Console.WriteLine("Introdueix un element per esborrar:");
            string element = Console.ReadLine().ToLower();
            try
            {
                if (!llista.Contains(element))
                {
                    throw new CustomExeceptions();
                }
                else
                {
                    llista.Remove(element);
                    Console.WriteLine("esborrat");
                }                                   
            }
            catch (CustomExeceptions e)
            {
                Console.WriteLine(e.RemoveElement);
            }
        }
        
    }
}
