﻿/*
 * Author:Pol
 * Date:3-4-2023
 */

namespace PolAlsinaP6M03UF5
{
    /// <summary>
    /// Clase per l'execucio del ex3
    /// </summary>
    internal class LlegirFitxer
    {
        /// <summary>
        /// intenta llegir un fitxer si no el troba llença una excepcio
        /// </summary>
        /// <param name="path">path on esta el fitxer</param>
        public static void llegirFitxer(string path)
        {
            try
            {
                string contingut = File.ReadAllText(path);
                Console.WriteLine(contingut);
            }
            catch (IOException e)
            {
                Console.WriteLine("S'ha produït un error d'entrada/sortida:");
                Console.WriteLine(e.Message);
            }
        }

    }
}
