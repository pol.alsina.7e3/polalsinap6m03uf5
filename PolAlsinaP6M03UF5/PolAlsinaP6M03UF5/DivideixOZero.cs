﻿/*
 * Author:Pol
 * Date:3-4-2023
 */


namespace PolAlsinaP6M03UF5
{
    /// <summary>
    /// Clase per l'execucio del ex1
    /// </summary>
    class DivideixOZero
    {
        /// <summary>
        /// metode que fa divisions
        /// </summary>
        /// <param name="dividend">dividend</param>
        /// <param name="divisor">divisor</param>
        /// <returns>retorna el resultat de l'operacio o l'execepcio amb un resultat 0</returns>
        public static double divideixoCero(int dividend, int divisor)
        {
            try
            {
                return dividend / divisor;
            }
            catch (ArithmeticException e)
            {
                Console.WriteLine(e.Message);
                return 0;
            }
        }

    }
}
