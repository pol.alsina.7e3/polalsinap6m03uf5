﻿/*
 * Author:Pol
 * Date:3-4-2023
 */
using PolAlsinaP6M03UF5;

/// <summary>
/// Clase main i per fer les proves del ex
/// </summary>
class Program
{
    /// <summary>
    /// Metode main per fer les proves
    /// </summary>
    static void Main()
    {
        ExDivideixzero();
        ExConvertirDouble();
        ExLlegirFitxer();
        ExAplcacioLliure();

    }
    /// <summary>
    /// proves del ex1
    /// </summary>
    static void ExDivideixzero()
    {
        Console.WriteLine("ExDivideixzero");
        Console.WriteLine(DivideixOZero.divideixoCero(7, 2));
        Console.WriteLine(DivideixOZero.divideixoCero(8, 4));
        Console.WriteLine(DivideixOZero.divideixoCero(5, 0));
        Console.WriteLine(DivideixOZero.divideixoCero(5, 0));
    }
    /// <summary>
    /// proves del ex2
    /// </summary>
    static void ExConvertirDouble()
    {
        Console.WriteLine("ExConvertirDouble");
        Console.WriteLine(ConvertirADouble.aDoubleoU("7,1"));
        Console.WriteLine(ConvertirADouble.aDoubleoU("9,"));
        Console.WriteLine(ConvertirADouble.aDoubleoU(",2"));
        Console.WriteLine(ConvertirADouble.aDoubleoU("tres"));
    } 
    /// <summary>
    /// prove del ex3
    /// </summary>
    static void ExLlegirFitxer()
    {
        Console.WriteLine("ExLlegirFitxer");
        LlegirFitxer.llegirFitxer("hola.txt");
    }
    /// <summary>
    /// proves del ex4
    /// </summary>
    static void ExAplcacioLliure()
    {
        Console.WriteLine("ExAplcacioLliure");
        AplicacioLliure al = new AplicacioLliure();
        al.MenuController();
    }

}
